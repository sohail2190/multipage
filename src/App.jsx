
import React from 'react';
import { BrowserRouter as Router, Routes,Route } from 'react-router-dom';
import About from './Components/about/about.component';
import Blog from './Components/blog/blog.component';
import Header from './Components/HeaderComp/header';
import Navbar from './Components/NavComp/navbar';

function App(){

  return(
    <>
    
    <Router>
      <Navbar/>
        <Routes>
          <Route path='/' Component={Header}></Route>
          <Route path='/about' Component={About}></Route>
          <Route path='/blog' Component={Blog}></Route>
        </Routes>
    </Router>
     
    </>
  )
}

export default App;

