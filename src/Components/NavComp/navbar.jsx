import React, { useRef } from 'react'
import './navbar.css'



const Navbar =()=> {
 
    const navRef = useRef();

    const showNavbar = ()=>{
        navRef.current.classList.toggle('show')
    }

  return (
      <>
        <nav>
		<div className="container" >
			<a href="#" className="brand">Logo</a>
			<ul className="nav-menu" ref={navRef}>
				<li><a href="/">Home</a></li>
				<li><a href="/about">About</a></li>
				<li><a href="/blog">Blog</a></li>
			
			
				
                 <div className='close_btn' onClick={showNavbar}>
                   <span className="material-symbols-outlined">close</span>
                 </div>
			</ul>
		</div>
           <div className='btn_toggle' onClick={showNavbar}>
                <span className="material-symbols-outlined">sort</span>
            </div>
       </nav>
      </>
  )
}

export default Navbar