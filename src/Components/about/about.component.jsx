import React from 'react';
import './style.css';
import  bgimg from '../../img/about.jpg'


const About = ()=>{
     return(<>
      <section className='about_us'>
         <div className='container'>
            <div className='about-text'>
                <h2>About Us </h2>
                <p>
                    lorem ipsum Some text that describes me lorem ipsum ipsum lorem
                    Some text that describes me lorem ipsum ipsum lorem 
                    Some text that describes me lorem ipsum ipsum lorem
                    Some text that describes me lorem ipsum ipsum lorem
                    Some text that describes.
                </p>
            </div>
            <div className='about_img'>
                 <img src={bgimg} />
            </div>
         </div>
      </section>
     
     </>)
}

export default About