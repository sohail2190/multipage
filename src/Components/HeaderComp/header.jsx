import React from 'react';
import './header.css';
import  bgimg from '../../img/profile.jpg'

const Header = ()=>{
    return(<>
      <header>
		<div class="container">
			<div class="left">
				<h1>Learn Code <br/> <span>Increase Your</span>  <br/> Knowledge</h1>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores dicta recusandae expedita maxime debitis nostrum possimus animi excepturi, fuga ipsum.</p>
				<a href="#" class="btn">Explore course</a>
			</div>
			<div class="right">
				<img src={bgimg} alt="Image"/>
			</div>
		</div>
	</header>
    </>)
}

export default Header